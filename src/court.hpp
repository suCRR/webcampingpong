/*
 * playfield.hpp
 *
 *  Created on: 14.02.2015
 *      Author: clemens
 */

#ifndef PLAYFIELD_HPP_
#define PLAYFIELD_HPP_

#include "PingPongClass.hpp"

class Court : public PingPongClass
{
private:
	Scalar color;
	int lineWidth;
	int width;
	int height;

public:
	Court(int lineWidth, Scalar color);
	void processFrame(Mat& Frame);
};


#endif /* PLAYFIELD_HPP_ */
