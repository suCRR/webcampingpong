#include <iostream>
#include <stdint.h>
#include <cmath>
#include <vector>

#include "PingPongClass.hpp"

#include "court.hpp"

class Square {
private:
	Point 	corners[4];

public:
	Point 	center;
	float 	lateralEdge;
	float 	diagonal;
	bool	valid;

	Square() {
		for( int i = 0; i < 4; i++) {
			corners[0] = Point(0,0);
		}
		center 		= Point(0,0);
		diagonal 	= 0.0f;
		lateralEdge = 0.0f;
		valid 		= false;
	}

	Square(vector<Point> &contour) {
		findSquare(contour);
	}

	void findSquare(vector<Point> &contour) {
		Point up(640,0);
		Point down(0,480);
		Point left(640,0);
		Point right(0,480);

		for(size_t k = 0; k < contour.size(); k++) {
			if(left.x  > contour[k].x) 	left 	= contour[k];
			if(right.x < contour[k].x) 	right 	= contour[k];
			if(up.y    < contour[k].y) 	up 		= contour[k];
			if(down.y  > contour[k].y) 	down 	= contour[k];
		}

		center.x = (up.x + left.x + down.x + right.x) / 4;
		center.y = (up.y + left.y + down.y + right.y) / 4;


		float distance_rd = norm(right-down);
		float distance_ur = norm(up-right);
		float distance_lu = norm(left-up);
		float distance_dl = norm(down-left);

		diagonal 	= ( norm(up-down) + norm(left-right)) /2;
		lateralEdge = (distance_lu + distance_ur + distance_rd + distance_dl) / 4;

		float distance_max = lateralEdge * 1.2f;
		float distance_min = lateralEdge * 0.8f;

		if( (distance_lu > distance_min) && (distance_lu < distance_max) &&
			(distance_dl > distance_min) && (distance_dl < distance_max) &&
			(distance_ur > distance_min) && (distance_ur < distance_max) &&
			(distance_rd > distance_min) && (distance_rd < distance_max) &&
			(lateralEdge > 8.0f)) {
			//cout << "left " << left << ", right "<< right << ", up "   << up <<	", down "	<< down << endl;

			corners[0] = left;
			corners[1] = right;
			corners[2] = up;
			corners[3] = down;

			valid = true;
		} else {
			valid = false;
		}
	}

	void draw(Mat &frame) {
		if(valid) {
			for(int i = 0; i < 4; i++) {
				circle(frame, corners[i], 7, Scalar(0,0,255), 3);
			}
			// diagonal
			line(frame, corners[0], corners[1], Scalar(255, 255, 255), 1);
			line(frame, corners[2], corners[3], Scalar(255, 255, 255), 1);

			// lateral edge
			line(frame, corners[1], corners[2], Scalar(0, 255, 0), 1);
			line(frame, corners[3], corners[0], Scalar(0, 255, 0), 1);
			line(frame, corners[1], corners[3], Scalar(0, 255, 0), 1);
			line(frame, corners[0], corners[2], Scalar(0, 255, 0), 1);

			// Center
			circle(frame, center, 5, Scalar(255, 0 ,0), 2);
		}
	}
};

class DiamondPattern {
public:
	Point center;
	Point otherCenter;
	float rotation;
	float length;

	bool valid;

	DiamondPattern(Square &A, Square &B) {
		findDiamond(A,B);
	}

	void findDiamond(Square &A, Square &B, float maxError=0.1f) {

		valid = false;

		if(A.valid && B.valid) {

			if( ((A.lateralEdge * 2 < B.lateralEdge*(1.0+maxError)) &&
				 (A.lateralEdge * 2 > B.lateralEdge*(1.0-maxError))) ) {

				center	 	= B.center;
				otherCenter = A.center;
				length 	 	= norm(B.center - A.center);
				rotation 	= atan2((float)(A.center.y - B.center.y), (float)(A.center.x - B.center.x)) * 180.0 / CV_PI;
				valid 		= true;
			}

			if( ((B.lateralEdge * 2 < A.lateralEdge*(1.0+maxError)) &&
				 (B.lateralEdge * 2 > A.lateralEdge*(1.0-maxError))) ) {
				center	 	= A.center;
				otherCenter = B.center;
				length 	 	= norm(A.center - B.center);
				rotation 	= atan2((float)(B.center.y - A.center.y), (float)(B.center.x - A.center.x)) * 180.0 / CV_PI;
				valid 		= true; //

			}

		}
	}

	void draw(Mat &frame) {
		line(frame, center, otherCenter, Scalar(255,0,0), 2, CV_AA);
		circle(frame, center, 3, Scalar(0,0,255), 2, CV_AA);
	}
};

class cannyEdgeDetector {
private:
	Mat frame_canny, frame_grey, frame_blur;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

public:
	vector<DiamondPattern> diamonds;

	void processFrame(Mat& frame) {
		vector<Square> squares;
		RNG rng(12345);

		int erosion_type = MORPH_RECT;
		Mat element = getStructuringElement(erosion_type,Size(4,4), Point(4/2,4/2));

		cvtColor(frame, frame_grey, CV_BGR2GRAY);
		GaussianBlur(frame_grey, frame_blur, Size(9,9), 2, 2);
		Canny(frame_blur, frame_canny, 0, 30, 3);

		//erode(frame_canny, frame_canny, element);

		/// Find contours
		findContours( frame_canny, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

		// Find squares
		for( size_t i = 0; i < contours.size(); i++) {
			Square s(contours[i]);
			if(s.valid) {
				//s.draw(frame);
				squares.push_back(s);
			}
		}

		diamonds.clear();
		for( size_t i = 0; i+1 < squares.size(); i++) {
			for( size_t j = i+1; j < squares.size(); j++) {
				DiamondPattern d(squares[i], squares[j]);
				if(d.valid) {
					diamonds.push_back(d);
				}
			}
		}
		cout << "Found " <<diamonds.size() << " diamonds."<<endl;

	}
};

class myRotatedRect : public RotatedRect {

private:

public:
	myRotatedRect() : RotatedRect() { }
	myRotatedRect( const Point2f& c, const Size2f& s, float a) : RotatedRect(c, s, a) {	}

	void draw(Mat frame, Scalar color, int thickness) {
		Point2f pts[4];
		this->points(pts);
		for( int j = 0; j < 4; j++ ) {
			line( frame, pts[j], pts[(j+1)%4], color, thickness );
		}
	}

	bool contains(const Point& pt) {
		vector<Point2f> cont;
		Point2f pts[4];
		this->points(pts);

		cont.push_back(pts[0]);
		cont.push_back(pts[1]);
		cont.push_back(pts[2]);
		cont.push_back(pts[3]);

		return (pointPolygonTest( cont ,pt, false) > 0) ? true : false;
	}

};



class playerClass : public PingPongClass {
private:

	Scalar	color;
	int 	num;
	int 	timeout;

	cannyEdgeDetector *detector;
public:
	myRotatedRect 	bar;
	bool	valid;

	//~playerClass() { }

	playerClass(cannyEdgeDetector *detPtr, int playerNum, Scalar col, int width, int height) {
		bar 		= myRotatedRect(Point(10,120),Size(width,height), 0.0f);
		detector 	= detPtr;
		num 		= playerNum;
		color 		= col;
		valid 		= false;
		timeout		= 0;
	}

	void processFrame(Mat &frame) {
		detector->processFrame(frame);
   // test
		timeout++;
		for( size_t i = 0; i < detector->diamonds.size(); i++) {
			if(((num == 0) && (detector->diamonds[i].rotation > 0)) || ( (num == 1) && (detector->diamonds[i].rotation < 0))) {
				bar.center	= detector->diamonds[i].center;
				valid 		= true;
				timeout		= 0;
				bar.angle	= (detector->diamonds[i].rotation + 90);
				cout << "Player " << num+1 << " rotation: " << detector->diamonds[i].rotation << endl;
				break;
			}
		}
		if(timeout < 20) {
			if(valid) {
				bar.draw(frame, color, 3);
			}
		} else {
			valid = false;
		}

	}

	bool hit(Point pt) {
		return bar.contains(pt);
	}
};

class scoreClass : public PingPongClass {

public:

	int scorePlayerOne;
	int scorePlayerTwo;

	//~scoreClass() { }
	scoreClass() {
		scorePlayerOne = scorePlayerTwo =0;
	}

	void processFrame(Mat &frame) {

		stringstream ss;

		ss << "Player Left: " << scorePlayerOne << "  Player Right: " << scorePlayerTwo;
		putText(frame, ss.str(),Point(10,230), 1, 1.15, Scalar(150,150,150), 1,CV_AA, false);
	}
};

class ballClass : public PingPongClass{

	int	 				pos[2];
	int					step[2];
	int					playfield[2];
	int 				size;
	Scalar				color;
	playerClass*		players[2];
	scoreClass*			scores;

public:

	//~ballClass() { }
	ballClass(Mat& frame, playerClass& oneRef, playerClass& twoRef, scoreClass& scoreRef) {
		pos[0] 		= frame.cols/2;
		pos[1] 		= frame.rows/2;
		step[0] 	= 10;
		step[1]		= 0;
		playfield[0] = frame.cols;
		playfield[1] = frame.rows;
		size 		= 5;
		color 		= Scalar(0,0,255);
		players[0]  = &oneRef;
		players[1]  = &twoRef;
		scores 		= &scoreRef;
	}


	void processFrame(Mat &frame) {
		playfield[0] = frame.cols;
		playfield[1] = frame.rows;

		if(players[0]->valid && players[1]->valid) {
			for(int i = 0; i < 2; i++) {
				pos[i] += step[i];
				if(pos[i] < 0) {
					if(i == 0) {
						pos[0] = 160;
						step[0] = -step[0];
						step[1] = 0;
						scores->scorePlayerTwo++;
					} else {
						pos[i] = -pos[i];
						step[i] = -step[i];
					}

				} else if(pos[i] > playfield[i]) {
					if(i == 0) {
						pos[0] = 160;
						step[0] = -step[0];
						step[1] = 0;
						scores->scorePlayerOne++;
					} else {
						pos[i] = (2 * playfield[i]) - pos[i];
						step[i] = -step[i];
					}
				} else if(players[0]->hit(Point(pos[0],pos[1]))) {
					cout << "Hit Player1" << endl;
					if(i == 0) {
						step[0] = -step[0];
						step[1] += players[0]->bar.angle - 90;
					}
				} else if(players[1]->hit(Point(pos[0],pos[1]))) {
					cout << "Hit Player1" << endl;
					if(i == 0) {
						step[0] = -step[0];
						step[1] += players[1]->bar.angle - 90;
					}
				}

			}
		} else {
			pos[0] = 160;
			pos[1] = 120;
			scores->scorePlayerOne = 0;
			scores->scorePlayerTwo = 0;
		}

		circle(frame, Point(pos[0], pos[1]),size, color,size/2, CV_AA);
	}

};



/** @function main */
int main(int argc, char** argv)
{
	// Create the default camera
	VideoCapture cap(0);
	Mat frame;
	cannyEdgeDetector detector;
	vector<PingPongClass*> objects;

	// Setup camera interface and get first frame
	if(!cap.isOpened()) { // check if we succeeded
    	cout << "Failed to open camera." << endl;
    	return -1;
    }
    cap.set(CV_CAP_PROP_FRAME_WIDTH, 640);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
	cap >> frame;

	// Player One
	objects.push_back(new playerClass(&detector, 0, Scalar(255, 0, 0), 10, 70));
	// Player Two
	objects.push_back(new playerClass(&detector, 1, Scalar(0, 255, 0), 10, 70));
	// Score
	objects.push_back(new scoreClass());
	// Ball
	objects.push_back(new ballClass(frame, (playerClass&) *objects[0],
									   (playerClass&) *objects[1],
									   (scoreClass&)  *objects[2]));
	// court
	objects.push_back(new Court(5, Scalar(0, 255, 0)));


    namedWindow("Camera Ping Pong",1);
    for(;;)
    {
        cap >> frame; // get a new frame from camera
        flip(frame,frame,1);
		// progress_timer t;

        for(vector<PingPongClass*>::iterator it = objects.begin(); it != objects.end(); ++it) {
        	(*it)->processFrame(frame);
        }

        imshow("Camera Ping Pong", frame);

        if(waitKey(1) >= 0) break;
    }

    return 0;
}
