/*
 * PingPongClass.hpp
 *
 *  Created on: 14.02.2015
 *      Author: clemens
 */

#ifndef PINGPONGCLASS_HPP_
#define PINGPONGCLASS_HPP_

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/features2d/features2d.hpp>

using namespace cv;
using namespace std;


class PingPongClass {
	public:
		virtual ~PingPongClass() {}
		virtual void processFrame(Mat& Frame)=0;
};



#endif /* PINGPONGCLASS_HPP_ */
