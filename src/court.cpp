#include "court.hpp"


Court::Court(int lineWidth, Scalar color)
{
	this->color		= color;
	this->lineWidth = lineWidth;
	width  = 0;
	height = 0;
}

void Court::processFrame(Mat &frame)
{
	Point pt1, pt2;

	if(!width || !height) {
		height = frame.rows;
		width  = frame.cols;
	}

	// Top
	line(frame, Point(0,lineWidth), Point(width,lineWidth), color, lineWidth);

	// Bottom
	line(frame, Point(0,height-lineWidth), Point(width,height-lineWidth), color, lineWidth);
}
