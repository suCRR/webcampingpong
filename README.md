WebcamPingPong
==============

Classic ping pong game using a webcam for user input. Based on OpenCV library.
example.png shows a screenshot of the game.

Author
==============
Clemens Reisner <cle.reisner(at)gmail.com>

